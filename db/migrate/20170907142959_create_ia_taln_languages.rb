# frozen_string_literal: true

class CreateIaTalnLanguages < ActiveRecord::Migration[5.1]
  def change
    create_table :ai_nlp_languages do |t|
      t.string :name
      t.text :map

      t.timestamps
    end
  end
end
