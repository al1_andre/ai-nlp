# frozen_string_literal: true

# Module containing artificial intelligence tools
module Ai
  # Module containing automatic natural language processing tools
  module Nlp
    # Main controller
    class ApplicationController < ActionController::Base
      protect_from_forgery with: :exception
    end
  end
end
