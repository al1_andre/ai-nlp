# frozen_string_literal: true

# Module containing artificial intelligence tools
module Ai
  # Module containing automatic natural language processing tools
  module Nlp
    # Manages a language
    class Language < ApplicationRecord
      validates_presence_of :name
      serialize :map, Hash

      ##
      # Add n-grams to the language
      # @param array given_array The array of [gram, frequancy]
      def add_grams(given_array)
        given_array.to_h.each do |key, freq|
          map[key] = map[key] ? letters + freq : freq
        end
        update
      end
    end
  end
end
