# frozen_string_literal: true

# Module containing artificial intelligence tools
module Ai
  # Module containing automatic natural language processing tools
  module Nlp
    # Classe mère
    class ApplicationRecord < ActiveRecord::Base
      self.abstract_class = true
    end
  end
end
