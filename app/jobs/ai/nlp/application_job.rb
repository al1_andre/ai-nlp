# frozen_string_literal: true

# Module containing artificial intelligence tools
module Ai
  # Module containing automatic natural language processing tools
  module Nlp
    # Main Job
    class ApplicationJob < ActiveJob::Base
    end
  end
end
