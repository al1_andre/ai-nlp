# frozen_string_literal: true

# Module containing artificial intelligence tools
module Ai
  # Module containing automatic natural language processing tools
  module Nlp
    # Main mailer
    class ApplicationMailer < ActionMailer::Base
      default from: "from@example.com"
      layout "mailer"
    end
  end
end
