# coding: utf-8
# frozen_string_literal: true

lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "ai/nlp/version"

Gem::Specification.new do |spec|
  spec.name          = "ai-nlp"
  spec.version       = Ai::Nlp::VERSION
  spec.authors       = ["Alain ANDRE"]
  spec.email         = ["dev@alain-andre.fr"]
  spec.summary       = "Artificial Intelligence and Automatic Natural Language Processing"
  spec.description   = <<END
  This gem contains a grouping of Ruby on Rails tools related to Artificial Intelligence
  and Automatic Natural Language Processing
END
  spec.homepage      = "https://gitlab.com/al1_andre/ai-nlp"
  spec.license       = "MIT"

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.required_ruby_version = ">= 2.3"
  spec.add_dependency "rails", "~> 5.1.3"
  spec.add_runtime_dependency "sanitize", "~> 4.5"
  spec.add_runtime_dependency "unicode", "~> 0.4.4.4"
end
