# frozen_string_literal: true

require "ai/nlp/engine"
require "ai/nlp/n_gram/n_gram"
require "ai/nlp/languages"

# Module containing artificial intelligence tools
module Ai
  # Module containing automatic natural language processing tools
  module Nlp
  end
end
