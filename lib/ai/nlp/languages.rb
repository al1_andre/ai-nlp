# encoding: utf-8
# frozen_string_literal: true

require "ai/nlp/n_gram/n_gram"

# Module containing artificial intelligence tools
module Ai
  # Module containing automatic natural language processing tools
  module Nlp
    # Class to handle multiple languages
    class Languages
      ##
      # Initialisation
      def initialize
        @n_gram = NGram.new
      end

      ##
      # Returns the currently known languages
      # @return An array of Language
      def all
        @languages = Language.all
      end

      ##
      # Offers among the available languages the closest one to the datasets
      # @param string input The data set.
      def guess(input)
        all
        return [] if @languages.empty?
        hash = @languages.map { |language| [language, score(input, language)] }.to_h
        sort(hash).reverse.map(&:name)
      end

      ##
      # Create a new language.
      # @param string name The language name.
      # @param string input The initial data set.
      # @return La langue créée.
      def create_one(name, input)
        language = Language.new(name: name)
        language.update(map: @n_gram.calculate(input).to_h)
      end

      private

        ##
        # Sort the language hash
        # @param hash hash The language hash
        # @return the sorted list of languages
        def sort(hash)
          sorted_languages = @languages.sort_by { |language| hash[language] }
          reject(sorted_languages, hash)
        end

        def reject(sorted_languages, hash)
          sorted_languages.reject { |language| hash[language].zero? }
        end

        ##
        # Compare a string of characters against a language based on, at most,
        # the 400 most commonly used groups of letters.
        # @param string input The data set to compare
        # @param Language language The Language to compare to
        def score(input, language)
          input_gram = @n_gram.calculate(input)
          ngram = language.map
          calculate_point([input_gram.size, 400].min, ngram, input_gram)
        end

        ##
        # Calculates the new frequency
        # @return le score (point)
        def calculate_point(max_compare, ngram, input_gram)
          point = 0
          (0..max_compare).each do |pos|
            position = input_gram[pos]
            next unless position
            point = add_frequency(ngram[position[0]], pos, point)
          end
          point
        end

        ##
        # Add frequency if needed
        # @param integer input_gram_freq The input gram frequency
        # @param integer pos The position in the max_compare
        # @param integer point The current calculated points
        def add_frequency(input_gram_freq, pos, point)
          point += (input_gram_freq - pos).abs if input_gram_freq
          point
        end
    end
  end
end
