# encoding: utf-8
# frozen_string_literal: true

require "sanitize"
require "cgi"
require "unicode"
require "byebug"

# Module containing artificial intelligence tools
module Ai
  # Module containing automatic natural language processing tools
  module Nlp
    # Class managing an n-gram hash
    class Hasher
      ##
      # Initialisation
      # @param string input The string to treat
      def initialize(input)
        @input = input
        @hash = {}
        clean
      end

      ##
      # Calculates n-gram frequencies for the dataset
      # @return Frequencies of ngram or sorted array
      def calculate
        @input.split(/[\d\s\[\]]/).each do |word|
          calculate_word_gram("_#{word}_")
        end
        drop_unwanted_keys
        @hash.sort { |one, other| other[1] <=> one[1] }
      end

      private

        ##
        # Enriched hash representing the n-gram of a word
        # @param string word The word to calculate
        def calculate_word_gram(word)
          length = word.size
          (0..length).each do |letter_position|
            parameters = { letter_position: letter_position, word: word, length: length }
            calculate_letter_gram(parameters)
            length -= 1
          end
        end

        ##
        # Deletes a key if its value is less than or equal to zero
        def drop_unwanted_keys
          @hash.each_key do |key|
            @hash.delete(key) if key.size <= 0
          end
        end

        ##
        # Stores the mono-gram, bi-gram and tri-gram in the hash
        # @param hash parameters The list of necessary parameters :
        #  - letter_position The position of the letter to be processed
        #  - word The word treated
        #  - length Current word size
        def calculate_letter_gram(parameters)
          (1..3).each do |nth|
            letters = parameters[:word][parameters[:letter_position], nth]
            next unless letters
            init_key(letters)
            @hash[letters] += 1 if parameters[:length] > (nth - 1)
          end
        end

        ##
        # Initialize key if necessary
        # @param string letters The group of letters
        def init_key(letters)
          @hash[letters] ||= 0
        end

        ##
        # Cleans the string passed as argument
        def clean
          safe_clean
          specific_clean
          clean_latin
          @input = @input.strip.split(" ").join(" ")
        end

        ##
        # Cleans the string from Latin characters if more than half of the string is not Latin.
        def clean_latin
          latin = @input.scan(/[a-z]/)
          nonlatin = @input.scan(/[\p{L}&&[^a-z]]/)
          nonlatin_ratio = nonlatin.size / (latin.size * 1.0)
          return if nonlatin_ratio < 0.5
          @input.gsub!(/[a-zA-Z]/, "") if !latin.empty? && !nonlatin.empty?
        end

        ##
        # Removes polluting web addresses, mails and characters
        def specific_clean
          uri_regex = %r/(?:http|https):\/\/[a-z0-9]+(?:[\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(?:(?::[0-9]{1,5})?\/[^\s]*)?/
          @input.gsub!(uri_regex, "")
          # Remove mails
          @input.gsub!(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}/, "")
          # Repleace polluting non-alphabetical characters, punctuation included by a space
          @input.gsub!(%r/[\*\^><!\"#\$%&\'\(\)\*\+:;,\._\/=\?@\{\}\[\]|\-\n\r0-9]/, " ")
        end

        ##
        # Cleaning via existing tools
        def safe_clean
          @input = Sanitize.clean(@input)
          @input = CGI.unescapeHTML(@input)
          @input = Unicode.downcase(@input)
        end
    end
  end
end
