# encoding: utf-8
# frozen_string_literal: true

require "ai/nlp/n_gram/hasher"

# Module containing artificial intelligence tools
module Ai
  # Module containing automatic natural language processing tools
  module Nlp
    # Class for calculating n-grams, storing and exploiting them
    class NGram
      ##
      # Cuts the data set into a grouping of letters
      # @param string input The dataset
      def hash(input)
        calculate(input).map { |letters, _gram| letters }
      end

      ##
      # Calculates the n-gram frequencies for the data set passed as an argument
      # @param string input The dataset
      # @return Frequencies of ngram or sorted array
      def calculate(input)
        hash = Hasher.new(input)
        hash.calculate
      end
    end
  end
end
