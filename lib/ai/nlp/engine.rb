# frozen_string_literal: true

# Module containing artificial intelligence tools
module Ai
  # Module containing automatic natural language processing tools
  module Nlp
    # Engine and isolation namespace
    class Engine < ::Rails::Engine
      isolate_namespace Ai::Nlp
    end
  end
end
