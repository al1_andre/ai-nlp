# frozen_string_literal: true

module Ai
  module Nlp
    VERSION = "0.1.1".freeze
  end
end
