[![pipeline status](https://gitlab.com/al1_andre/ai-nlp/badges/master/build.svg)](https://gitlab.com/al1_andre/ai-nlp/commits/master)
[![Coverage report](https://gitlab.com/al1_andre/ai-nlp/badges/master/coverage.svg?job=rspec)](http://al1_andre.gitlab.io/ai-nlp)
[![Gem Version](https://badge.fury.io/rb/ai-nlp.svg)](https://badge.fury.io/rb/ai-nlp)

# Object
This gem contains a grouping of Ruby on Rails tools related to Artificial Intelligence and Automatic Natural Language Processing.

It aim to allow a rails way of using NLP.

# Installing
Put in your Gemfile the following line `gem 'ai-nlp'`

```
bundle install
rails ai_nlp:install:migrations
```
# Testing
The test is done by running `./bin/rails test`

# Covering
It currently allows to learn detecting languages.

```ruby
$ @languages = Ai::Nlp::Languages.new
#<Ai::Nlp::Languages:0x000000052061b0 @n_gram=#<Ai::Nlp::NGram:0x00000005206110>>
$ @languages.create_one('french', "Salut mon pote, comment ça va ? Plutôt bien !")
true
$ @languages.create_one('english', "Hello bro, how are you ? Fine !")
true
$ @languages.guess("salut")
["french", "english"]
```

# References
 - [AI4R](https://github.com/SergioFierens/ai4r/)
 - [rubynlp](http://rubynlp.org/)
 