# frozen_string_literal: true

Rails.application.routes.draw do
  mount Ai::Nlp::Engine => "/ai-nlp"
end
