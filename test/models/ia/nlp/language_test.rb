# frozen_string_literal: true

require "test_helper"

module Ai::Nlp
  class LanguageTest < ActiveSupport::TestCase
    test "It should be created with a name" do
      assert Ai::Nlp::Language.create!(name: "test")
    end

    test "It should not be created without a name" do
      assert_raises do
        Ai::Nlp::Language.create!()
      end
    end

    test "It should contain a hash" do
      language = Ai::Nlp::Language.create!(name: "test", map: { _l: 1 })
      assert_equal 1, language.map[:_l]
    end
  end
end
