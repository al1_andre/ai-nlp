# encoding: utf-8
# frozen_string_literal: true

require "test_helper"

module Ai
  # Minitest du module Nlp
  module Nlp
    # Minitest de la classe NGram
    class LanguagesTest < ActiveSupport::TestCase
      setup do
        @languages = Ai::Nlp::Languages.new
      end

      test "It returns all existing languages" do
        assert_equal 2, @languages.all.count
      end

      test "It returns 0 for a not existing language" do
        assert_equal 0, @languages.guess("Bon").count
      end

      test "It creates a new language" do
        assert @languages.create_one("french", "salut")
      end

      test "It finds an existing language" do
        @languages.create_one("french", "Salut mon pote, comment ça va ? Plutôt bien !")
        @languages.create_one("english", "Hello bro, how are you ? Fine !")
        assert_equal ["french", "english"], @languages.guess("salut")
      end
    end
  end
end
