# encoding: utf-8
# frozen_string_literal: true

require "test_helper"

module Ai
  # Minitest du module Nlp
  module Nlp
    # Minitest de la classe NGram
    class NGramTest < ActiveSupport::TestCase
      setup do
        @bad_text = "***Hello*** Go to http://www.youtube.com to watch some shitty videos."
        @bad_text += ">>> Woooooo <<< <a href='blah.com/no'>friend</a> WIN TODAY!!!!"
        @bad_text += "???? @#!!(**%#)} [[}}||]]"
        @n_gram = Ai::Nlp::NGram.new
      end

      test "Remove characters that throw off language detection" do
        assert_equal(@n_gram.hash(@bad_text),
                     %w[_ o t o_ oo e i ooo h _t _to to _w w s d l to_ a _s y
                        y_ n _h _he he hel el ell ll llo lo lo_ _g _go g go go_
                        _wa wa wat at atc tc tch c ch ch_ h_ _so so som om ome m
                        me me_ e_ _sh sh shi hi hit it itt tt tty ty ty_ _v _vi
                        v vi vid id ide de deo eo eos os os_ s_ _wo wo woo oo_ _f
                        _fr f fr fri r ri rie ie ien en end nd nd_ d_ _wi wi win
                        in in_ n_ tod od oda da day ay ay_])
      end

      test "Create an 2d array of [letters, frequency] for a given text input" do
        assert_equal(@n_gram.calculate("Bon"),
                     [["_", 2], ["_b", 1], ["_bo", 1], ["b", 1], ["bo", 1],
                      ["bon", 1], ["o", 1], ["on", 1], ["on_", 1], ["n", 1],
                      ["n_", 1]])
      end

      test "Create a array of letters for a given text input" do
        assert_equal(@n_gram.hash("Bon"), %w[_ _b _bo b bo bon o on on_ n n_])
      end

      test "Consider this string latin" do
        assert_equal(@n_gram.hash("Ça va ?"), %w[_ a a_ _ç _ça ç ça ça_ _v _va v va va_])
      end

      test "Consider this string notlatin" do
        assert_equal(@n_gram.hash("Et oui : éèà@ùë"), %w[_ _é _éè é éè éèà è èà èà_ à à_ _ù _ùë ù ùë ùë_ ë ë_])
      end
    end
  end
end
