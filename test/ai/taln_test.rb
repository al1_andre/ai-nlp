# frozen_string_literal: true

require "test_helper"

class Ai::Nlp::Test < ActiveSupport::TestCase
  test "truth" do
    assert_kind_of Module, Ai::Nlp
  end
end
