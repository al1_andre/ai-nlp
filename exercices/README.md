# Objet
Ce répertoire contient des exercices permettant de comprendre et d’appréhender des concepts de l'IA.

# TALN - Traitement automatique du langage naturel ou NLP en anglais
> Le TALN est une discipline à la frontière de la linguistique, de l'informatique et de l'intelligence artificielle, qui concerne l'application de programmes et techniques informatiques à tous les aspects du langage humain

[TALN](https://fr.wikipedia.org/wiki/Traitement_automatique_du_langage_naturel)

## Reconnaissance de langue
La syntaxe d'une langue et celle d'une autre différent, la grammaire ainsi que la conjugaison aussi. Avant tout traitement sur le contenu, il est donc nécessaire de définir la langue que l'on traite.

L'une des façons de déterminer la langue est de calculer le **N-gramme** d'une phrase... Le **quoi** ?

Le [N-gramme](https://fr.wikipedia.org/wiki/N-gramme) ! En gros c'est un calcul qui vous permettrait de gagner plus vite au jeu du pendu ! Il s'agit de la probabilité d'apparition d'une **sous-séquence** à partir d'une **autre** sous-séquence.

Par exemple, le **bi-gramme** le plus fréquent de la langue française est "**de**". La probabilité d'obtenir la sous-séquence **e** après avoir eu la sous-séquence **d** est donc très élevée !! Vous avez saisi ?

Pour cet exercice, je vais utiliser une classe basique permettant de calculer le n-gramme d'un jeu de données.

Commençons notre expérience !

```
$ ./console
> n_gram = Ai::Nlp::NGram.new
 => #<Ai::Nlp::NGram:0x000000027c0ce8>
> n_gram.calculate('Bonjour toi !')
 => [["_", 4], ["o", 3], ["_b", 1], ["_bo", 1], ["b", 1], ["bo", 1], ["bon", 1], ["on", 1], ["onj", 1], ["n", 1], ["nj", 1], ["njo", 1], ["j", 1], ["jo", 1], ["jou", 1], ["ou", 1], ["our", 1], ["u", 1], ["ur", 1], ["ur_", 1], ["r", 1], ["r_", 1], ["_t", 1], ["_to", 1], ["t", 1], ["to", 1], ["toi", 1], ["oi", 1], ["oi_", 1], ["i", 1], ["i_", 1]]
```

Le résultat nous indique qu'il y a quatre **espaces**, trois **o**, un **_b** etc. Ce groupe de lettres **_b** signifie que c'est un **b** qui commence un mot. Donc pour le cas du `["oi_", 1]`, vous comprenez qu'il s'agit d'un bi-gramme de fin de mot et qu'il a été trouvé une fois.

Je vais faire en sorte que mon programme apprenne au fil de l'eau. C'est une forme d'IA car au début, le programme n'est pas capable de déterminer la langue utilisée. Il faut l’entraîner !

Toujours dans la console, je vais utiliser la classe `Languages`.

```
$ langues = Ai::Nlp::Languages.new
 => #<Ai::Nlp::Languages:0x00000001a4da48 @manager=#<Ai::Nlp::FileManager:0x00000001a4cda0>, @n_gram=#<Ai::Nlp::NGram:0x00000001a4cd78>> 
$ langues.find('salut')
 => []
$ langues.all
 => []
$ langues.create('french', 'Salut mon pote, comme ça va ?')
```
 
## Découpage sémantique

[tokenizer](https://github.com/diasks2/pragmatic_tokenizer)

[lemmatizer](https://github.com/yohasebe/lemmatizer)